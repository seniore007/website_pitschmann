<?php
/**
 * The template for showing our footer
 *
 * @package Motive
 * @since 1.0
 */
?>

		<?php zilla_content_end(); ?>
		<!-- END #content .site-content-->
		</div>

		<?php if( ! is_singular('portfolio') ) {
			get_sidebar('footer');
		} ?>

	<!-- END #container .hfeed .site -->
	</div>

		<?php zilla_footer_before(); ?>
		<!-- BEGIN #footer -->
		<footer id="footer" class="fade-in one site" role="contentinfo">
		<div class="site-footer clearfix">
		<?php zilla_footer_start(); ?>

			<p class="copyright">&copy; <?php echo date( 'Y' ); ?> <a href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a> WordPress theme by <a href="http://www.themezilla.com/">Themezilla</a></p>

		<?php zilla_footer_end(); ?>
		<!-- END #footer -->
		</div>
		</footer>
		<?php zilla_footer_after(); ?>

	<!-- Theme Hook -->
	<?php wp_footer(); ?>
	<?php zilla_body_end(); ?>

	<!-- <?php echo 'Ran '. $wpdb->num_queries .' queries '. timer_stop(0, 2) .' seconds'; ?> -->
<!--END body-->
</body>
<!--END html-->
</html>