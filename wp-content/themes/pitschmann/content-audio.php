<?php
/**
 * The template to display post content for audio post formats
 *
 * @package Motive
 * @since 1.0
 */
$theme_options = get_theme_mod('zilla_theme_options');
zilla_post_before(); ?>
<!--BEGIN .post -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php zilla_post_start(); ?>

	<?php if ( is_singular() ) {
		echo base_print_audio_html($post->ID);
	} else {
		base_post_thumbnail($post->ID);
	} ?>

	<!--BEGIN .entry-header-->
	<header class="entry-header">
		<?php
		base_post_title();
		base_post_meta_header();
		?>
	<!--END .entry-header-->
	</header>

	<?php if( is_singular() ) {

		base_the_content();
		base_post_footer();

	} else { ?>

		<!--BEGIN .entry-summary -->
		<div class="entry-summary">
			<?php
				if ($theme_options['general_blog_layout'] === 'layout-standard') {
					if ( has_excerpt() ) {
				    	echo '<div class="excerpt">';
				    	the_excerpt();
				    	echo '</div>';
					} else { 
						the_content();
					}	
				} else {
					the_excerpt();
				}
			?>
		<!--END .entry-summary -->
		</div>
		
		<?php base_comments_link(); ?>

	<?php } ?>

<?php zilla_post_end(); ?>
<!--END .post-->
</article>
<?php zilla_post_after(); ?>