<?php
/**
 * The main template file
 *
 * @package Motive
 * @since 1.0
 */
$theme_options = get_theme_mod('zilla_theme_options');
get_header(); ?>

	<!--BEGIN #primary .site-main-->
	<div id="primary" class="site-main" role="main">
	<?php if( have_posts() ) : ?>
		<?php $layout = isset($theme_options['general_blog_layout']) ? $theme_options['general_blog_layout'] : 'layout-masonry'; ?>
		<div class="post-container <?php echo $layout; ?>" id="post-container">
		<?php while (have_posts()) : the_post();

			get_template_part('content', get_post_format() );

		endwhile; ?>
		</div>
		<?php base_paging_nav();

	else :

		get_template_part('content', 'none');

	endif; ?>
	<!--END #primary .site-main-->
	</div>

<?php get_footer(); ?>