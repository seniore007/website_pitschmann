<?php
/**
 * Template to show portfolios from a portfolio type
 *
 * @package  Base
 * @since  1.0
 */

get_header(); ?>

	<!--BEGIN #primary .site-main-->
	<div id="primary" class="site-main" role="main">

		<?php
		$terms = get_terms( 'portfolio-type', array('hierarchical' => false) );
		if( count($terms) ){
			echo '<div class="filter-toggle"><ul class="portfolio-type-nav"  style="display:none;">';
			echo '<li><a href="#" data-filter="*" class="active">#'. __( 'All', 'zilla' ) .'</a></li>';
			foreach( $terms as $term ) {
				$output = '<li><a href="'. get_term_link($term) .'" data-filter=".term-'. $term->slug .'">#'. $term->name .'</a>';
				if( $term !== end($terms) ) $output .= ' ';
				echo $output .'</li>';
			}
			echo '</ul></div>';
		}
		?>

	<?php if( have_posts() ) : ?>

		<header class="archive-header">
			<h1 class="archive-title"><?php single_term_title(); ?></h1>
			<?php echo term_description(); ?>
		</header>

		<div id="portfolio-feed" class="portfolio-feed cs-style-3 clearfix">

			<?php while( have_posts() ) : the_post() ; ?>

				<?php get_template_part( 'content', 'portfolio' ); ?>

			<?php endwhile;

				base_paging_nav();

			?>

		</div>

	<?php else :

		get_template_part( 'content', 'none' );

	endif; ?>

	<!--END #primary .site-main-->
	</div>

<?php get_footer(); ?>