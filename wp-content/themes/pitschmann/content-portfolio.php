<?php
/**
 * The template to display portfolio content
 *
 * @package Motive
 * @since 1.0
 */

$terms = get_the_terms( $post->ID, 'portfolio-type' );
$term_list = '';
if( !empty($terms) ) {
	foreach( $terms as $term ) {
		$term_list .= 'term-'. $term->slug .' ';
	}
	$term_list = trim($term_list);
}

zilla_post_before(); ?>
<!--BEGIN .post -->

<?php if( is_singular('portfolio') ) { ?>
		<?php
		$terms = get_terms( 'portfolio-type', array('hierarchical' => false) );
		if( count($terms) ){
			echo '<div class="filter-toggle"><ul class="portfolio-type-nav"  style="display:none;">';
			echo '<li><a href="#" data-filter="*" class="active">#'. __( 'All', 'zilla' ) .'</a></li>';
			foreach( $terms as $term ) {
				$output = '<li><a href="#'. get_term_link($term) .'" data-filter=".term-'. $term->slug .'">'. $term->name .'</a>';
				if( $term !== end($terms) ) $output .= ' ';
				echo $output .'</li>';
			}
			echo '</ul></div>';
		}
		?>
<?php } ?>

<article id="post-<?php the_ID(); ?>" <?php post_class($term_list); ?>>
<?php zilla_post_start(); ?>

	<?php if( is_singular('portfolio') ) { ?>

		<?php
		$portfolio_custom_fields = array();
		$portfolio_custom_fields['display_gallery'] = get_post_meta( $post->ID, '_tzp_display_gallery', true );
		$portfolio_custom_fields['display_audio'] = get_post_meta( $post->ID, '_tzp_display_audio', true );
		$portfolio_custom_fields['display_video'] = get_post_meta( $post->ID, '_tzp_display_video', true );
		?>

		<!--BEGIN .entry-header-->
		<?php base_portfolio_media_feature($post->ID, $portfolio_custom_fields); ?>
		<header class="entry-header">
			<?php
			base_post_title();
			base_portfolio_meta( $post->ID );
			?>
		<!--END .entry-header-->
		</header>

		<?php base_the_content(); ?>

		<footer class="entry-footer">
			<div class="entry-tags">
				<?php
					$terms = get_the_terms( $post->ID, 'portfolio-type' );
					if( !empty($terms) ) {
						foreach( $terms as $term ) {
							echo '<a href="'. get_term_link( $term ) .'">'. $term->name .'</a>';
						}
					}
				?>
			</div>
		</footer>

		<?php base_portfolio_media($post->ID, $portfolio_custom_fields); ?>

	<?php } else { ?>
			
		<div class="entry-thumbnail fade-in two">
			<a href="<?php the_permalink(); ?>"><?php
				if(has_post_thumbnail()) {
					the_post_thumbnail('portfolio-thumb');
				} ?></a>
			<div class="caption">
				<h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<?php the_terms($post->ID, 'portfolio-type', '<div class="portfolio-types entry-meta">', ', ', '</div>'); ?>
			</div>
		</div>

	<?php } ?>

<?php zilla_post_end(); ?>
<!--END .post-->
</article>
<?php zilla_post_after(); ?>