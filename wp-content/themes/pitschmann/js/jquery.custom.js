/*-----------------------------------------------------------------------------------

 	Custom JS - All front-end jQuery

-----------------------------------------------------------------------------------*/

;(function($) {
	"use strict";

	$(document).ready(function($) {

		var $window = $(window);
		
		/* --------------------------------------- */
		/* Overlay Menu & Superfish
		/* --------------------------------------- */
			
		$(document).on('keyup', function(e) {
			if (e.keyCode == 27) {
				if ($('.overlay').hasClass('open')) {
					$('.overlay-close').trigger('click');
				}
			}
	  });
	  
	  $('#primary-menu .sub-menu').hide(); //Hide children by default
	
		$('#primary-menu li a').click(function(event){
			if ($(this).next('ul.sub-menu').children().length !== 0) {     
		    event.preventDefault();
			}
			$(this).siblings('.sub-menu').slideToggle(150);
		}); 

		/* --------------------------------------- */
		/* Animated Header
		/* --------------------------------------- */

		var ptnavOpen = $('.portfolio-type-nav').is('.open');
		var ptnavWasOpen = false;

		var zillaAnimatedHeader = (function() {

			var docElem = document.documentElement,
				header = document.querySelector( '.site-header' ),
				didScroll = false,
				changeHeaderOn = 250;

			function init() {
				window.addEventListener( 'scroll', function( event ) {
					if( !didScroll ) {
						didScroll = true;
						setTimeout( scrollPage, 250 );
						}
					}, false );
			}

			function scrollPage() {
				var sy = scrollY();
				if ( sy >= changeHeaderOn ) {
					classie.add( header, 'site-header-shrink' );
					if ( ptnavOpen === true ){
						ptnavWasOpen = true;
						$('.portfolio-type-nav').slideToggle(150).toggleClass('open');
						$('#container').toggleClass('pt-nav-open')
						ptnavOpen = $('.portfolio-type-nav').is('.open');
					}
				}
				else {
					if ( ptnavWasOpen === true && classie.has( header, 'site-header-shrink' ) ){
						$('.portfolio-type-nav').slideDown(150).addClass('open');
						$('#container').addClass('pt-nav-open')
						ptnavOpen = $('.portfolio-type-nav').is('.open');
					}
					classie.remove( header, 'site-header-shrink' );
				}
				didScroll = false;
			}

			function scrollY() {
				return window.pageYOffset || docElem.scrollTop;
			}

			init();

		})();	

		/* --------------------------------------- */
		/* Cycle - Slideshow media
		/* --------------------------------------- */
		function initSlideshows() {
		
			if( $().cycle ) {
				var $sliders = $('.slideshow');
				$sliders.each(function() {
					var $this = $(this),
						prev = $this.next().find('.zilla-slide-prev'),
						next = $this.next().find('.zilla-slide-next');
	
					$this.cycle({
						autoHeight: 0,
						slides: '> li',
						swipe: true,
						timeout: 0,
						prev: prev,
						next: next
					});
	
					if( $('body').hasClass('single-post') ) {
						$this.on('cycle-update-view', function(e,o,sh,cs) {
							var $cs = $(cs);
							$(this).animate({
								height: $cs.height()
							}, 300);
						});
					}
				});
			}
			
		}

		initSlideshows();

    /* ------------------------------------------------- */
    /* Isotope
    /* ------------------------------------------------- */
    	function staggeredFadeIn($selector) {
	    	$selector.each(function (i) {
				var $item = $(this);
				window.setTimeout(function () {
					$item.addClass('show');
				}, i*100);
			});
    	}
    	
    	function setIntervalX(callback, delay, repetitions) {
		    var x = 0;
		    var intervalID = window.setInterval(function () {
		
		       callback();
		
		       if (++x === repetitions) {
		           window.clearInterval(intervalID);
		       }
		    }, delay);
		}
		
	    if( $().isotope ) {
			var $container = $('#portfolio-feed'),
				$blogContainer = $('.layout-masonry'),
				infiniteCount = 0;
	
			if($container.length){
				$container.imagesLoaded(function(){
					
					staggeredFadeIn($container.find('.type-portfolio'));
					
					$container.isotope({
						itemSelector: '.type-portfolio',
						layoutMode: 'fitRows',
						hiddenStyle: {
							opacity: 0
						},
						visibleStyle: {
							opacity: 1
						}
					});
				});
	
				$('.portfolio-type-nav a').on('click', function(e){
					e.preventDefault();
					$container.isotope({
					    filter: $(this).attr('data-filter')
					});
					$('.portfolio-type-nav a').removeClass('active');
					$(this).addClass('active');
				});
			}
	
			if($blogContainer.length){
				$blogContainer.imagesLoaded(function(){
					
					staggeredFadeIn($blogContainer.find('[class^="post"]'));
					
					$blogContainer.isotope({
						itemSelector: 'article',
						layoutMode: 'masonry',
						hiddenStyle: {
							opacity: 0
						},
						visibleStyle: {
							opacity: 1
						}
					});
					
					//relayout after videos load
					$('video').on('loadeddata', function () {
						$blogContainer.isotope( 'layout' );	
					});
				});
			}
			
			
			// Infinite Scroll
			$( document.body ).on( 'post-load', function () {
				// Identify the new posts as all elements between the two bottom-most .infinite-loader elements
				var $allElems = $('.post-container').find('.post');
				var $firstLoader = $('.infinite-loader:eq(' + infiniteCount + ')');
				infiniteCount++;
				var $secondLoader = $('.infinite-loader:eq(' + infiniteCount + ')');
				var $newElems = $firstLoader.nextUntil($secondLoader);
	
				// Prevent showing the new posts before slideshows are initialized and elements are added to isotope
				$newElems.css({ display : 'none' }).addClass('show');
				
				initSlideshows();
		
				// Once all images are loaded, add the new elements to isotope
				$allElems.imagesLoaded( function() {
					
					$blogContainer.isotope( 'insert', $newElems );
					
					//hack to get the slideshows to layout properly
					setIntervalX(function () {
					    $blogContainer.isotope( 'layout' );
					}, 200, 25);
				});
			});
			
			
		}

		$('#filter-toggle').click(function() {
			$('.portfolio-type-nav').slideToggle(150).toggleClass('open');
			$('#container').toggleClass('pt-nav-open')
			ptnavOpen = $('.portfolio-type-nav').is('.open');
			ptnavWasOpen = $('.portfolio-type-nav').is('.open');
			return false;
		});       
		
		 

		/* --------------------------------------- */
		/* Responsive media - FitVids
		/* --------------------------------------- */
		if( $().fitVids ) {
			$('#content').fitVids();
		} /* FitVids --- */

	});

})(window.jQuery);