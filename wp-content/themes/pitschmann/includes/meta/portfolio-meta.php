<?php

/**
 * Create the Portfolio meta boxes
 */
 
add_action('add_meta_boxes', 'zilla_metabox_portfolio');
function zilla_metabox_portfolio(){   
	
	/* Create an image metabox -------------------------------------------------------*/
	$meta_box = array(
		'id' => 'zilla-metabox-portfolio-thumbnail',
		'title' =>  __('Zilla Portfolio Thumbnail', 'zilla'),
		'description' => __('Upload an image 360px by 360px to display on the portfolio page.', 'zilla'),
		'page' => 'portfolio',
		'context' => 'normal',
		'priority' => 'default',
		'fields' => array(
			array(
				'name' =>  __('Upload Image', 'zilla'),
				'desc' => __('Edit the image by clicking upload.', 'zilla'),
				'id' => '_zilla_portfolio_thumb',
				'type' => 'images',
				'std' => __('Upload Image', 'zilla')
			)
		)
	);
    zilla_add_meta_box( $meta_box );
    

}