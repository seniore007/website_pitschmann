# Motive Changelog

2015-01-08 **v1.0.1** Nic Oliver <nic@themezilla.com>

- */header.php* $theme_options['general_text_logo']: Updated the if statement around the custom logo script, which was intermittently returning an error.
- */style.css*: Updated version number.

2015-01-08 **v1.0** Jesse Campbell <jesse@themezilla.com>

- First release