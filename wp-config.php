<?php
/**
 * In dieser Datei werden die Grundeinstellungen für WordPress vorgenommen.
 *
 * Zu diesen Einstellungen gehören: MySQL-Zugangsdaten, Tabellenpräfix,
 * Secret-Keys, Sprache und ABSPATH. Mehr Informationen zur wp-config.php gibt es
 * auf der {@link http://codex.wordpress.org/Editing_wp-config.php wp-config.php editieren}
 * Seite im Codex. Die Informationen für die MySQL-Datenbank bekommst du von deinem Webhoster.
 *
 * Diese Datei wird von der wp-config.php-Erzeugungsroutine verwendet. Sie wird ausgeführt,
 * wenn noch keine wp-config.php (aber eine wp-config-sample.php) vorhanden ist,
 * und die Installationsroutine (/wp-admin/install.php) aufgerufen wird.
 * Man kann aber auch direkt in dieser Datei alle Eingaben vornehmen und sie von
 * wp-config-sample.php in wp-config.php umbenennen und die Installation starten.
 *
 * @package WordPress
 */

/**  MySQL Einstellungen - diese Angaben bekommst du von deinem Webhoster. */
/**  Ersetze database_name_here mit dem Namen der Datenbank, die du verwenden möchtest. */
define('DB_NAME', 'pitschmann2015');

/** Ersetze username_here mit deinem MySQL-Datenbank-Benutzernamen */
define('DB_USER', 'pitschmann.admin');

/** Ersetze password_here mit deinem MySQL-Passwort */
define('DB_PASSWORD', '34PHddF4');

/** Ersetze localhost mit der MySQL-Serveradresse */
define('DB_HOST', '192.168.100.102');

/** Der Datenbankzeichensatz der beim Erstellen der Datenbanktabellen verwendet werden soll */
define('DB_CHARSET', 'utf8');

/** Der collate type sollte nicht geändert werden */
define('DB_COLLATE', '');

/**#@+
 * Sicherheitsschlüssel
 *
 * Ändere jeden KEY in eine beliebige, möglichst einzigartige Phrase.
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * kannst du dir alle KEYS generieren lassen.
 * Bitte trage für jeden KEY eine eigene Phrase ein. Du kannst die Schlüssel jederzeit wieder ändern,
 * alle angemeldeten Benutzer müssen sich danach erneut anmelden.
 *
 * @seit 2.6.0
 */
define('AUTH_KEY',         'H6]jqOs**/F|eJ_pVRCVYyp<erh=5^vmxZs^!|-jhGS)Dd(Rs)Mz1OS(TN&Y,,pO');
define('SECURE_AUTH_KEY',  '_v=|R}?j+PuH@}TVlc:*4ZKO%7fwO$|.Y;?{]^9FBFA2L)6||B{m};|05b}]l*}|');
define('LOGGED_IN_KEY',    'e3E||!X-9:ouiQQV4p.sPps%R +yT)v:P[*df2Nd;ar:*V<=5VX32%S 4PR @yD4');
define('NONCE_KEY',        '[;8A?>(G{fr.I+j|,*zqATX>BiKY||fG+drL/V`eaX%|Dvy _ z7QrE@pQ2=4B^<');
define('AUTH_SALT',        'E2 ZZ;^&:Z^r2u^(P+W|~@)N7XH_>ppg|sbu0<,b+;G|<v<6.4-4`07;h)S[r5{2');
define('SECURE_AUTH_SALT', '}!6et9=5sP_;-+#M7^^PDJ O6Z7>W!).kaHei;SE,QWt)&MT{d&l:!q$_KIKS8lD');
define('LOGGED_IN_SALT',   ')FEi&T+T),0X=Bp(V2H>7uhX^Ruri1l&^mfxJ #Ke@,msk6F Y57F|^(*`eBO=}5');
define('NONCE_SALT',       '&a|jim_xvFM,>]uRyF/C=]|&}8Nee3pT)8=7<qi]1rbuBk~rsQU);?KH`)|HLXr9');

/**#@-*/

/**
 * WordPress Datenbanktabellen-Präfix
 *
 *  Wenn du verschiedene Präfixe benutzt, kannst du innerhalb einer Datenbank
 *  verschiedene WordPress-Installationen betreiben. Nur Zahlen, Buchstaben und Unterstriche bitte!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
